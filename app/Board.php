<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $fillable = ['thread_id'];
    protected $guarded = ['id', 'created_at', 'update_at'];

    public function thread()
    {
        return $this->hasMany('App\Thread');
    }

}
