<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = ['user_id', 'image','title','text'];
    protected $guarded = ['id', 'created_at', 'update_at'];

    function thread() {
        return $this->belongsTo('App\Thread');
    }

}
