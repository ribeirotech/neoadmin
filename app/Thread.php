<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $fillable = ['post_id'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    
    public function post()
    {
        return $this->hasMany('App\Post');
    }

    function board() {
        return $this->belongsTo('App\Board');
    }

}
