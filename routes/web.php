<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/api/boards', 'BoardController@index');
Route::get('/api/boards/{board_name}', 'BoardController@show');

Route::get('/api/threads', 'ThreadController@index');
Route::get('/api/threads/{thread_id}', 'ThreadController@show');
Route::post('/api/threads', 'ThreadController@store');

Route::get('/api/posts', 'PostController@index');
Route::get('/api/posts/{post_id}', 'PostController@show');
Route::post('/api/posts', 'PostController@store');